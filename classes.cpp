#include <iostream>

class Example
{
private:
    int a = 1;
    int b = 2;

public:
    void Show()
    {
        std::cout << '\n' << a << ' ' << b << '\n';
    }

};

class Vector
{
private:
    double x;
    double y;
    double z;
    double j = pow(x,2) + pow(y, 2) + pow(z, 2);
public:
    Vector() : x(2), y(4), z(8)
    {}
    void Show()
    {
        std::cout << "\n" << pow (x, 2) << ' ' << pow (y, 2) << ' ' << pow(z, 2) << "\n";
        std::cout << "\n" << sqrt(j);
    }
    
};


int main()
{
    Example k;
    k.Show();

    Vector v;
    v.Show();
}
